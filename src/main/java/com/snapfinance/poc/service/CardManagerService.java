package com.snapfinance.poc.service;

import com.snapfinance.poc.dto.AddCardRequest;
import com.snapfinance.poc.enums.AddCardStatusEnum;
import com.snapfinance.poc.enums.PCIProviderOptionsEnum;
import com.snapfinance.poc.enums.SnapProductTypeEnum;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class CardManagerService
{

    public boolean addCard( AddCardRequest addCardRequest )
    {

        if ( !this.isCardValid( addCardRequest ) )
        {
            return false;
        }
        if ( !this.isPCIProviderOptionsEnumValid( addCardRequest.getPciProvider() ) )
        {
            return false;
        }
        if ( !this.isSnapProductTypeEnumValid( addCardRequest.getProductType() ) )
        {
            return false;
        }
        // Mock some paymentGateway or storeCardGateway
        AddCardStatusEnum addCardStatusEnum = this.storeCard( addCardRequest );
        return AddCardStatusEnum.SUCCESS.equals( addCardStatusEnum );

    }

    private boolean isCardValid( AddCardRequest addCardRequest )
    {
        try
        {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "MM/yy" );
            Date expirationDateObj = simpleDateFormat.parse( addCardRequest.getCardExpiration().trim() );
            Date today = new Date();
            return !addCardRequest.getCardholderName().isBlank() && addCardRequest.getCardNumber().length() >= 15 && addCardRequest.getCardNumber().length() <= 16 && !addCardRequest.getCardCvc().isBlank() && today.before( expirationDateObj );
        }
        catch ( ParseException e )
        {
            return false;
        }
    }

    private boolean isPCIProviderOptionsEnumValid(String pciProvider)
    {
        if ( !StringUtils.hasText( pciProvider ) )
        {
            return false;
        }
        try
        {
            PCIProviderOptionsEnum.valueOf( pciProvider );
            return true;
        }
        catch( IllegalArgumentException e)
        {
            return false;
        }
    }

    private boolean isSnapProductTypeEnumValid(String snapProductType)
    {
        if ( !StringUtils.hasText( snapProductType ) )
        {
            return false;
        }
        try
        {
            SnapProductTypeEnum.valueOf( snapProductType );
            return true;
        }
        catch( IllegalArgumentException e)
        {
            return false;
        }
    }

    public AddCardStatusEnum storeCard( AddCardRequest addCardRequest )
    {
        SnapProductTypeEnum snapProductTypeEnum;
        PCIProviderOptionsEnum pciProviderOptionsEnum;
        try
        {
            snapProductTypeEnum = SnapProductTypeEnum.valueOf( addCardRequest.getProductType() );
            pciProviderOptionsEnum = PCIProviderOptionsEnum.valueOf( addCardRequest.getPciProvider() );
        }
        catch ( IllegalArgumentException e )
        {
            throw new UnsupportedOperationException("Payment or snap product are not valid");
        }

        if ( SnapProductTypeEnum.RTO.equals( snapProductTypeEnum ) || SnapProductTypeEnum.SSL.equals( snapProductTypeEnum ) )
        {
            switch ( pciProviderOptionsEnum )
            {
                case NON_PROVIDER:
                    //Store in Payix or Repay
                    return AddCardStatusEnum.SUCCESS;
                case ECKOH:
                    return AddCardStatusEnum.SUCCESS;
                default:
                    throw new UnsupportedOperationException( String.format( "No payment gateway implementation available yet for %s", pciProviderOptionsEnum.name() ) );
            }
        }
        else
        {
            if ( SnapProductTypeEnum.SLN.equals( snapProductTypeEnum ) )
            {
                //Use pagix to store the card
                return AddCardStatusEnum.SUCCESS;
            }
        }
        return AddCardStatusEnum.FAIL;
    }

}
