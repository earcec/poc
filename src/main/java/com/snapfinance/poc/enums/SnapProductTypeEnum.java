package com.snapfinance.poc.enums;

public enum SnapProductTypeEnum
{
    SSL( "Credit+" ),
    RTO( "Finance" ),
    SLN( "Snap Loan" );

    private String pDisplay;

    SnapProductTypeEnum( String pDisplay )
    {
        this.pDisplay = pDisplay;
    }

    public String getpDisplay()
    {
        return pDisplay;
    }

    public void setpDisplay( String pDisplay )
    {
        this.pDisplay = pDisplay;
    }
}
