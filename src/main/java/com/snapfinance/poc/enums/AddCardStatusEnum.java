package com.snapfinance.poc.enums;

public enum AddCardStatusEnum
{
    SUCCESS( "Card was added succesfully" ),
    FAIL( "An error ocurred, please check the card information" );

    private String displayText;

    AddCardStatusEnum( String displayText )
    {
        this.displayText = displayText;
    }

    public String getDisplayText()
    {
        return displayText;
    }

    public void setDisplayText( String displayText )
    {
        this.displayText = displayText;
    }
}
