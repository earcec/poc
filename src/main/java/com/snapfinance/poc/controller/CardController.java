package com.snapfinance.poc.controller;

import com.snapfinance.poc.dto.AddCardRequest;
import com.snapfinance.poc.dto.AddCardResponse;
import com.snapfinance.poc.enums.AddCardStatusEnum;
import com.snapfinance.poc.service.CardManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping( "/api/v1/card" )
public class CardController
{

    @Autowired
    private CardManagerService cardManagerService;

    @GetMapping( "/ping" )
    public ResponseEntity<String> getPing()
    {
        return ResponseEntity.ok( "pong" );
    }

    @PostMapping( "/addCard" )
    public ResponseEntity<AddCardResponse> addCard( @RequestBody @Valid AddCardRequest addCardRequest )
    {
        boolean addCardResult = cardManagerService.addCard( addCardRequest );
        AddCardResponse addCardResponse;
        if ( addCardResult )
        {
            addCardResponse = new AddCardResponse( addCardResult, AddCardStatusEnum.SUCCESS.getDisplayText() );
        }
        else
        {
            addCardResponse = new AddCardResponse( addCardResult, AddCardStatusEnum.FAIL.getDisplayText() );
        }
        return ResponseEntity.ok( addCardResponse );
    }

}
