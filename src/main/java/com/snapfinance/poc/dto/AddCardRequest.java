package com.snapfinance.poc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.snapfinance.poc.enums.PCIProviderOptionsEnum;
import com.snapfinance.poc.enums.SnapProductTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddCardRequest implements Serializable
{

    @JsonProperty("application_id")
    @NotBlank
    private String applicationId;

    @JsonProperty( "cardholder_name" )
    @NotBlank
    private String cardholderName;

    @JsonProperty( "card_number" )
    @NotBlank
    private String cardNumber;

    @JsonProperty( "card_cvc" )
    @NotBlank
    private String cardCvc;

    @JsonProperty( "card_expiration" )
    @NotBlank
    private String cardExpiration;

    @JsonProperty( "billing_zip" )
    @NotBlank
    private String billingZip;
    
    @JsonProperty( "PCI_provider" )
    @NotNull
    private String pciProvider;

    @JsonProperty( "product_type" )
    @NotNull
    private String productType;
}
