package com.snapfinance.poc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddCardResponse implements Serializable
{

    @NonNull
    private boolean result;

    @NonNull
    private String message;
}
